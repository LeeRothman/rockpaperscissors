﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RockPaperScissors.Service;
using RockPaperScissors.ViewModels;
using RockPaperScissors.WebUI.Controllers;
using System.Web.Mvc;

namespace RockPaperScissors.WebUI.Tests.Controllers
{
    [TestClass]
    public class GameControllerTests
    {
        private GameController gameController;
        private Mock<IGameService> mockService;

        [TestInitialize]
        public void InitTest()
        {
            mockService = new Mock<IGameService>();
            gameController = new GameController(mockService.Object);
        }

        [TestMethod]
        public void Create_Get_actionresult_has_view()
        {
            ViewResult result = gameController.Create() as ViewResult;
            
            Assert.AreEqual("Create", result.ViewName);
        }

        [TestMethod]
        public void PlayGame_calls_service_method()
        {
            mockService.Setup(m => m.PlayGame(It.IsAny<GameViewModel>()));

            gameController.PlayGame(new GameViewModel());

            mockService.Verify(m => m.PlayGame(It.IsAny<GameViewModel>()), Times.Once);
        }
    }
}
