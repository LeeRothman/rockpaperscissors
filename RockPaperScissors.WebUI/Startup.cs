﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(RockPaperScissors.WebUI.Startup))]
namespace RockPaperScissors.WebUI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
