﻿using RockPaperScissors.Service;
using RockPaperScissors.ViewModels;
using System.Web.Mvc;

namespace RockPaperScissors.WebUI.Controllers
{
    public class GameController : Controller
    {
        private readonly IGameService gameService;

        public GameController(IGameService gameService)
        {
            this.gameService = gameService;
        }

        public ViewResult Create()
        {
            return View("Create", new GameViewModel());
        }

        public JsonResult PlayGame(GameViewModel viewModel)
        {
            gameService.PlayGame(viewModel);

            return Json(viewModel);
        }

    }
}