﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RockPaperScissors.ViewModels;

namespace RockPaperScissors.Service.Tests
{
    [TestClass]
    public class GameServiceTests
    {
        private Mock<IGame> mockGame;
        private GameService gameService;

        [TestInitialize]
        public void TestInit()
        {
            mockGame = new Mock<IGame>();
            gameService = new GameService(mockGame.Object);
        }

        [TestMethod]
        public void PlayGame_calls_Game_roll_method()
        {
            mockGame.Setup(m => m.Roll(It.IsAny<Hand>(), It.IsAny<Hand>()));            

            gameService.PlayGame(new GameViewModel());

            mockGame.Verify(m => m.Roll(It.IsAny<Hand>(), It.IsAny<Hand>()), Times.Once);
        }

        [TestMethod]
        public void PlayGame_sets_viewmodel_result()
        {
            mockGame.Setup(m => m.Roll(It.IsAny<Hand>(), It.IsAny<Hand>())).Returns(PlayResult.FirstPlayer);

            GameViewModel viewModel = new GameViewModel();

            gameService.PlayGame(viewModel);

            Assert.AreEqual(viewModel.Result, "FirstPlayer");
        }
    }
}
