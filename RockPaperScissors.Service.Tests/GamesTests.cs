﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace RockPaperScissors.Service.Tests
{
    [TestClass]
    public class GamesTests
    {
        private Game game;

        [TestInitialize]
        public void TestInit()
        {
            game = new Game();
        }

        [TestMethod]
        public void two_rocks_tie()
        {
            Assert.AreEqual(PlayResult.Tie, game.Roll(Hand.Rock, Hand.Rock));
        }

        [TestMethod]
        public void two_papers_tie()
        {
            Assert.AreEqual(PlayResult.Tie, game.Roll(Hand.Paper, Hand.Paper));
        }

        [TestMethod]
        public void two_scissors_tie()
        {
            Assert.AreEqual(PlayResult.Tie, game.Roll(Hand.Scissors, Hand.Scissors));
        }

        [TestMethod]
        public void scissors_beats_paper_player_one()
        {
            Assert.AreEqual(PlayResult.FirstPlayer, game.Roll(Hand.Scissors, Hand.Paper));
        }

        [TestMethod]
        public void scissors_beats_paper_player_two()
        {
            Assert.AreEqual(PlayResult.SecondPlayer, game.Roll(Hand.Paper, Hand.Scissors));
        }

        [TestMethod]
        public void paper_beats_rock_player_one()
        {
            Assert.AreEqual(PlayResult.FirstPlayer, game.Roll(Hand.Paper, Hand.Rock));
        }

        [TestMethod]
        public void paper_beats_rock_player_two()
        {
            Assert.AreEqual(PlayResult.SecondPlayer, game.Roll(Hand.Rock, Hand.Paper));
        }

        [TestMethod]
        public void rock_beats_scissors_player_one()
        {
            Assert.AreEqual(PlayResult.FirstPlayer, game.Roll(Hand.Rock, Hand.Scissors));
        }

        [TestMethod]
        public void rock_beats_scissors_player_two()
        {
            Assert.AreEqual(PlayResult.SecondPlayer, game.Roll(Hand.Scissors, Hand.Rock));
        }
    }
}
