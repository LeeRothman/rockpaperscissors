﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace RockPaperScissors.Service.Tests
{
    [TestClass]
    public class ComputerPlaysComputerTests
    {
        [TestMethod]
        public void computer_plays_computer_gives_result()
        {
            Player.ComputerPlayer playerOne = new Player.ComputerPlayer();
            Player.ComputerPlayer playerTwo = new Player.ComputerPlayer();

            Game game = new Game();

            PlayResult result = game.Roll(playerOne.GetHand(), playerTwo.GetHand());

            Assert.IsInstanceOfType(result, typeof(PlayResult));
        }
    }
}
