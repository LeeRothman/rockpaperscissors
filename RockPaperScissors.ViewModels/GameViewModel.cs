﻿
namespace RockPaperScissors.ViewModels
{
    public class GameViewModel
    {
        public string Result { get; set; }
        public string ResultText { get; set; }
    }
}
