﻿using RockPaperScissors.Service.Player;
using RockPaperScissors.ViewModels;

namespace RockPaperScissors.Service
{
    public class GameService : IGameService
    {
        private IGame game;

        public GameService(IGame game)
        {
            this.game = game;
        }

        public void PlayGame(GameViewModel viewModel)
        {
            ComputerPlayer playerOne = new ComputerPlayer();
            ComputerPlayer playerTwo = new ComputerPlayer();

            Hand playerOneHand = playerOne.GetHand();
            Hand playerTwoHand = playerTwo.GetHand();

            PlayResult result = game.Roll(playerOneHand, playerTwoHand);

            viewModel.Result = result.ToString();

            BuildResultText(viewModel, result, playerOneHand, playerTwoHand);
        }

        private static void BuildResultText(GameViewModel viewModel, PlayResult result, Hand playerOneHand, Hand playerTwoHand)
        {
            viewModel.ResultText = viewModel.Result;

            if (result.Equals(PlayResult.FirstPlayer))
            {
                viewModel.ResultText += " wins " + playerOneHand + " beats " + playerTwoHand;
            }
            else if (result.Equals(PlayResult.SecondPlayer))
            {
                viewModel.ResultText += " wins " + playerTwoHand + " beats " + playerOneHand;
            }
        }
    }
}
