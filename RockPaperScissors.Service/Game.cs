﻿
namespace RockPaperScissors.Service
{
    public class Game : IGame
    {
        public PlayResult Roll(Hand first, Hand second)
        {
            PlayResult result;

            if (first == second)
            {
                result = PlayResult.Tie;
            }
            else if ((SecondHasScissorsFirstHasPaper(first, second)) || (SecondHasPaperFirstHasRock(first, second)) || (SecondHasRockFirstHasScissors(first, second)))
            {
                result = PlayResult.SecondPlayer;
            }
            else
            {
                result = PlayResult.FirstPlayer;
            }

            return result;
        }

        private static bool SecondHasRockFirstHasScissors(Hand first, Hand second)
        {
            return second == Hand.Rock && first == Hand.Scissors;
        }

        private static bool SecondHasPaperFirstHasRock(Hand first, Hand second)
        {
            return second == Hand.Paper && first == Hand.Rock;
        }

        private static bool SecondHasScissorsFirstHasPaper(Hand first, Hand second)
        {
            return second == Hand.Scissors && first == Hand.Paper;
        }
    }
}
