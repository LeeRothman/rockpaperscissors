﻿
namespace RockPaperScissors.Service
{
    public enum Hand
    {
        Rock = 0,
        Paper = 1,
        Scissors = 2
    }
}
