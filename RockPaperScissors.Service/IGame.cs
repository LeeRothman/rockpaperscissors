﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RockPaperScissors.Service
{
    public interface IGame
    {
        PlayResult Roll(Hand first, Hand second);
    }
}
