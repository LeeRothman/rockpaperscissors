using System.Web.Mvc;
using Microsoft.Practices.Unity;
using Unity.Mvc5;

namespace RockPaperScissors.Service
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();
            
            // register all your components with the container here
            // it is NOT necessary to register your controllers
            
            // e.g. container.RegisterType<ITestService, TestService>();
            container.RegisterType<IGameService, GameService>();

            container.RegisterType<IGame, Game>();
            
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}