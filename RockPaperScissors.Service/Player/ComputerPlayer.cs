﻿using System;

namespace RockPaperScissors.Service.Player
{
    public class ComputerPlayer : IPlayer
    {
        private static Random random;

        public ComputerPlayer()
        {
            random = new Random();
        }

        public Hand GetHand()
        {
            int randomNumber = random.Next(0, 3);

            Hand playersHand = (Hand)Enum.GetValues(typeof(Hand)).GetValue(randomNumber);

            return playersHand;
        }
    }
}
