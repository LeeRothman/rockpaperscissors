﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RockPaperScissors.ViewModels;

namespace RockPaperScissors.Service
{
    public interface IGameService
    {
        void PlayGame(GameViewModel viewModel);
    }
}
