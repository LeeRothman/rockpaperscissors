﻿
namespace RockPaperScissors.Service
{
    public enum PlayResult
    {
        Tie,
        FirstPlayer,
        SecondPlayer
    }
}
